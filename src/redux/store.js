import {createStore, applyMiddleware} from 'redux'
import api from './middlewares/api'

import reducer from './reducers';

export const store = createStore(reducer, applyMiddleware(api));

