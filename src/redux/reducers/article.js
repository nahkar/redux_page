export const INCREMENT = 'INCREMENT'
export const DECREMENT = 'DECREMENT'
export const LOAD_ARTICLES = 'LOAD_ARTICLES'
export const LOAD_PAGE = 'LOAD_PAGE'
export const SUCCESS = 'SUCCESS'
export const FAIL = 'FAIL'

export function increment(){
    return {
        type: INCREMENT
    }
}
export function decrement(){
    return {
        type: DECREMENT
    }
}
export function loadArticles(id = 1){
    return {
        type: LOAD_ARTICLES,
        payload:{
            id
        }
    }
}
export function loadSuccessArticles(data){
    return {
        type: LOAD_ARTICLES + SUCCESS,
        payload: {
            data
        }
    }
}
export function loadFailArticles(){
    return {
        type: LOAD_ARTICLES + FAIL
    }
}
export function loadPage(id){
    return {
        type: LOAD_PAGE,
        payload:{
            id
        }
    }
}
export function loadSuccessPage(data){
    return {
        type: LOAD_PAGE + SUCCESS,
        payload:{
            data
        }
    }
}
export function loadFailPage(){
    return {
        type: LOAD_PAGE + FAIL
    }
}
const defaultState = {
    count: 1,
    articles: {
        loading: false,
        data: [],
        page:{
            title:'',
            body:''
        }
    }
}


export default (state = defaultState, action)=>{
    const {type, payload} = action;
    switch (type){
        case INCREMENT:
            return {...state, count: state.count + 1}
        case DECREMENT:
            return {...state, count: state.count - 1}
        case LOAD_ARTICLES:
            return {...state, articles: { ...state.articles, loading: true}}
        case LOAD_ARTICLES + SUCCESS:
            return {...state, articles: {loading: false, data: payload.data}}
        case LOAD_ARTICLES + FAIL:
            return {...state, articles: { ...state.articles, loading: false}}
        case LOAD_PAGE:
            return state
        case LOAD_PAGE + SUCCESS:
            return {...state, articles: { ...state.articles, loading: false, page: {title: payload.data.title, body: payload.data.bodySingle}}}
            return state
        case LOAD_PAGE + FAIL:
            return state
        default:
            return state
    }
}