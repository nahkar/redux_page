import {LOAD_ARTICLES, LOAD_PAGE, loadFailArticles, loadSuccessArticles, loadSuccessPage, loadFailPage} from '../reducers/article'

export default store => next => action =>{
    if(action.type === LOAD_ARTICLES){
        const {id} = action.payload
        setTimeout(()=>{
            fetch(`https://hochu-family.firebaseio.com/hochu-family_life/items/page/${id}.json`)
                .then((response)=> {
                    return response.json();
                }).then((data)=> {
                    store.dispatch(loadSuccessArticles(data))
                })
                .catch((err)=> {
                    store.dispatch(loadFailArticles())
                });
        }, 1000)
    }else if(action.type === LOAD_PAGE){
        const {id} = action.payload
        fetch(`https://hochu-family.firebaseio.com/hochu-family_life/single/${id}.json`)
            .then((response)=> {
                return response.json();
            }).then((data)=> {
                store.dispatch(loadSuccessPage(data))
            })
            .catch((err)=> {
                store.dispatch(loadFailPage())
            });
    }
    next(action);
}