import React, { Component } from 'react';
export default class Article extends Component {

    state = {
        activePage: null,
        open: true
    }

    addCount = (e) =>{
        e.preventDefault();
        this.props.increment();
        this.props.loadArticles(this.props.count + 1)
    }

    deductCount = (e) => {
        e.preventDefault()
        if(this.props.count > 1 ){
            this.props.decrement();
            this.props.loadArticles(this.props.count - 1)
        }
    }

    openPage = (id) =>{
        if(id === this.state.activePage){
            this.setState({
                open: false
            })
        }else{
            this.props.loadPage(id)
            this.setState({
                activePage: id,
                open: true
            })
        }

    }

    componentDidMount(){
        this.props.loadArticles()
    }

    renderArticles = () => {
        if(this.props.articles.length){
            const data = this.props.articles.map((el)=>{
                return (
                    <li onClick={this.openPage.bind(null, el.link)} key={el.link}>
                        <a className="list-group-item" href="#">{el.title}</a>
                        {(this.props.page && this.state.open && this.state.activePage === el.link) ? <div className="jumbotron" dangerouslySetInnerHTML={{__html: this.props.page.body}}></div> : null}
                    </li>
                )
            })
            return(
                <ul className="list-group">
                    <li className="text-center"><a href="#" className="list-group-item disabled">Articles page №{this.props.count}</a></li>
                    {data}
                </ul>
            )
        }else if(!this.props.loading){
            return <h1>Doesn't articles</h1>
        }
    }

    render() {

        return (
            <div>
                { this.props.loading ? <h1 className="loading">Loading .... </h1> : this.renderArticles() }
                <nav aria-label="...">
                    <ul className="pager">
                        <li className="previous"><a onClick={this.deductCount} href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
                        <li className="next"><a onClick={this.addCount} href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
                    </ul>
                </nav>
            </div>
        );
    }
}
