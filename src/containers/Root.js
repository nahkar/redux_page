import React, { Component } from 'react';
import ArticleList from './ArticleList'
import {Provider} from 'react-redux'
import {store} from '../redux/store'

export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <ArticleList/>
            </Provider>
        )
    }s
}
