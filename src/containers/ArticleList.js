import React, { Component } from 'react';
import Article from '../components/Article'
import {increment, loadArticles, decrement, loadPage} from  '../redux/reducers/article'
import {connect} from 'react-redux'

class ArticleList extends Component {
    render() {
        return <Article
                count={this.props.count}
                loadArticles={this.props.loadArticles}
                increment={this.props.increment}
                loading={this.props.loading}
                articles={this.props.articles}
                decrement={this.props.decrement}
                loadPage={this.props.loadPage}
                page={this.props.page}
        />
    }
}

export default connect((store)=>{
        return {
            count: store.article.count,
            loading: store.article.articles.loading,
            articles: store.article.articles.data,
            page: store.article.articles.page
        }
    },{
        increment,
        loadArticles,
        decrement,
        loadPage
    })(ArticleList)